## TF modules are flattened here to reduce number of files

terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.31.0"
    }
  }
}

provider "aws" {
  region  = "us-east-1"
  profile = "default"
}

## Resources

resource "aws_iam_group" "prod-ci" {
  name = "prod-ci"
  path = "/users/"
}

resource "aws_iam_user" "user_one" {
  name = "test-user"
}

resource "aws_iam_user" "user_two" {
  name = "test-user-two"
}

resource "aws_iam_group_membership" "team" {
  name = "tf-testing-group-membership"

  users = [
    aws_iam_user.user_one.name,
    aws_iam_user.user_two.name,
  ]

  group = aws_iam_group.prod-ci.name
}

## IAM section

resource "aws_iam_role" "noperms" {
  name = "noperms"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
      },
    ]
  })
}

resource "aws_iam_policy" "noperms" {
  name   = "noperms_policy"
  role   = aws_iam_role.noperms.id
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
      },
    ]
  })
}

resource "aws_iam_role_policy_attachment" "noperms-attach" {
  name       = "noperms-attachment"
  role       = aws_iam_role.noperms.name
  policy_arn = aws_iam_policy.noperms.arn
  groups     = [aws_iam_group.prod-ci.name]
}