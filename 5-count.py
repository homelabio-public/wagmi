#!/usr/bin/env python3

## Use like a shell script, ex: ./5-count.py < log.txt

import re
import sys

iplist = {}  # create an empty dict

for line in sys.stdin:
    line = line.strip()
    if line:
        iplist.setdefault(line, 0)
        iplist[line] += 1

for key in iplist.keys():
    line = "%-15s = %s" % (key, iplist[key])
    print(f'{line}')
