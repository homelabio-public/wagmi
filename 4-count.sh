#!/bin/bash

## Use like a simple unix tool, ex: ./4-count.py < log.txt

uniq -c - | sort -k 1nr,1 | awk '{print $1" "$3}'
