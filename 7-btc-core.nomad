job "btc-core" {
    datacenters   = ["dc1"]
    type          = "service"
    namespace     = "crypto"
    group "btc-core" {
        network {
            mode = "bridge"
            port "http-rpc" {}
            port "tcp-p2p" {}
        }
        reschedule {
            delay          = "2m"
            delay_function = "constant"
            unlimited      = true
        }
        update {
            max_parallel      = 1
            health_check      = "checks"
            min_healthy_time  = "10s"
            healthy_deadline  = "30m"
            progress_deadline = "60m"
            auto_revert       = true
            stagger           = "10s"
        }
        ephemeral_disk {
            migrate = true
            size    = 500000
            sticky  = true
        }
        service {
            name             = "btc-core-rpc"
            port             = "http-rpc"
            address_mode     = "host"
            check {
                name         = "healthcheck"
                type         = "http"
                port         = "http-rpc"
                path         = "/v2/info"
                interval     = "10s"
                timeout      = "2s"
                address_mode = "alloc"
            }
        }
        service {
            name         = "btc-core-p2p"
            port         = "tcp-p2p"
            address_mode = "host"
        }
        task "btc-core" {
            driver = "docker"
            resources {
                cpu    = 250
                memory = 500
            }
            config {
                image   = "local/btc:22.0"
                args    = [
                            "-blocksdir=${NOMAD_ALLOC_DIR}/data",
                            "-port=${NOMAD_PORT_tcp-p2p}",
                            "-rpcport=${NOMAD_PORT_http-rpc}",
                ]
            }
        }
    }
}
